#!/opt/homebrew/bin/python3

from brownie import SpellRepayer, accounts


def main():
    SpellRepayer[0].updateStrategyPendingRewards({'from': accounts[2]})
    tx = SpellRepayer[0].pending_rewards()
    print(tx)
    txb = SpellRepayer[0].available_rewards()
    print(txb)
    SpellRepayer[0].strategyHarvest({'from': accounts[2]})
    txd = SpellRepayer[0].pending_rewards()
    print(txd)
    txe = SpellRepayer[0].available_rewards()
    print(txe)
    txf = SpellRepayer[0].getUserShare({'from': accounts[2]})
    print(txf.return_value)
    txg = SpellRepayer[0].total_mSPELL({'from': accounts[2]})
    print(txg)
    txh = SpellRepayer[0].getUserAmount({'from': accounts[2]})
    print(txh.return_value)
    # txa = SpellRepayer[0].getUserAmount({'from': accounts[2]})
    # print(txa.return_value)
    # txb = SpellRepayer[0].getUserShare({'from': accounts[2]})
    # print(txb.return_value)
    # SpellRepayer[0].withdraw(30000,{'from': accounts[1]})
    # txc = SpellRepayer[0].total_mSPELL()
    # print(txc)
    # txd = SpellRepayer[0].getUserAmount({'from': accounts[1]})
    # print(txd.return_value)
