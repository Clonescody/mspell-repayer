#!/opt/homebrew/bin/python3

from brownie import SpellRepayer, accounts

def main():
    accounts[0].deploy(SpellRepayer)
    SpellRepayer[0].deposit(90000, {'from': accounts[1]})
    SpellRepayer[0].deposit(10000, {'from': accounts[2]})
