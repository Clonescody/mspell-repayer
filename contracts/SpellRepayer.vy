# @version ^0.3.1

# Contract SpellRepayer

"""
@title Spell Repayer
@author Clonescody
@notice
    This contract allow a user to deposit their SPELL tokens, so they
    can be staked as mSPELL. Periodically, the contract is called to 
    harvest the MIM rewards. User can then use his share of the contract's accumulated MIMs to
    repay one of it's debt at Abracadabra.
"""

interface MSPELL:
    def deposit(amount: uint256): nonpayable
    def withdraw(amount: uint256): nonpayable
    def pendingReward(user: address) -> uint256: view

interface ERC20: 
    def transfer(to: address, amount: uint256) -> bool: nonpayable

mSPELL_ADDRESS: constant(address) = 0xbD2fBaf2dc95bD78Cf1cD3c5235B33D1165E6797
SPELL_ADDRESS: constant(address) = 0x090185f2135308BaD17527004364eBcC2D37e5F6
MIM_ADDRESS: constant(address) = 0x99D8a9C45b2ecA8864373A26D1459e3Dff1e17F3

contract_mSPELL: MSPELL
contract_SPELL: ERC20
contract_MIM: ERC20

mSpellBalanceOf: HashMap[address, uint256]

total_mSPELL: public(uint256)
available_rewards: public(uint256)
pending_rewards: public(uint256)

@internal
@nonpayable
def _sendMims(user: address, amount: uint256):
    """
    @notice Sends amount of MIMs to user
    @param user The user to transfer the MIMs to
    @param amount The amount of MIMs to tranfer
    """
    self.contract_MIM.transfer(user, amount)
    

@internal
@nonpayable
def _sendSpell(user: address, amount: uint256):
    """
    @notice Sends amount of SPELL to user
    @param user The user to transfer the SPELLs to
    @param amount The amount of SPELLs to tranfer
    """
    self.contract_SPELL.transfer(user, amount)

@internal
@nonpayable
def _deposit(user: address, amount: uint256):
    """
    @notice Sends amount of MIMs to user
    @param user The user to transfer the MIMs to
    @param amount The amount of MIMs to tranfer
    """
    self.mSpellBalanceOf[user] += amount
    self.total_mSPELL += amount
    # self.contract_mSPELL.deposit(amount)
    # response: Bytes[32] = raw_call(
    #     token,
    #     concat(
    #         method_id("transfer(address,uint256)"),
    #         convert(receiver, bytes32),
    #         convert(amount, bytes32),
    #     ),
    #     max_outsize=32,
    # )

@internal
@view
def _getUserShare(user: address) -> uint256:
    """
    @notice Get the ownership share of user as a %
    @param user The user to get the share of the strategy
    @return User's share of the strategy
    """
    userShare: uint256 = ((self.mSpellBalanceOf[user] / self.total_mSPELL) * 100)
    return userShare

@internal
@view
def _getUserClaimableAmount(user: address) -> uint256:
    """
    @notice Get the amount of MIMs claimable by user
    @param user The user to get the share of rewards of
    @return User's claimable amount of MIMs
    """
    assert self.mSpellBalanceOf[user] > 0
    user_claimable_amount: uint256 = (self.pending_rewards + self.available_rewards) * (self._getUserShare(user) / 100)
    assert user_claimable_amount <= self.pending_rewards + self.available_rewards
    return user_claimable_amount

@internal
@nonpayable
def _updateStrategyPendingRewards():
    """
    @notice Updates the strategy's amount of MIMs pending for harvest
    @dev This should be called on each distribution in the mSPELL staking pool
    """
    # self.contract_mSPELL.pendingReward(self)
    pending_rewards_call: uint256 = 1234
    assert pending_rewards_call != self.pending_rewards
    self.pending_rewards = pending_rewards_call

@internal
@nonpayable
def _strategyHarvest():
    """
    @notice Harvest MIMs rewards of the strategy
    @dev use real amount instead of anticipated
    """
    assert self.pending_rewards > 0
    # self.contract_mSPELL.withdraw(0)
    self.available_rewards += self.pending_rewards
    self.pending_rewards = 0

@internal
@nonpayable
def _claimAndSend(user: address):
    """
    @notice Claim MIM's share of user
    @param user User to transfer the MIMs to
    """
    user_claimable_amount: uint256 = self._getUserClaimableAmount(user)
    if user_claimable_amount > 0:
        if user_claimable_amount <= self.available_rewards + self.pending_rewards:
            if user_claimable_amount > self.available_rewards:
                self._strategyHarvest()
            # assert maybe not needed ?
            assert self.available_rewards >= user_claimable_amount
            self._sendMims(user, user_claimable_amount)

@internal
@nonpayable
def _withdraw(user: address, amount: uint256):
    """
    @notice Withdraw the SPELL and rewards of an user
    @param user User address
    @param amount Amount of SPELL to withdraw
    """
    self.contract_mSPELL.withdraw(amount)
    received_spell_from_mspell: uint256 = 1234
    assert received_spell_from_mspell == amount
    
    self._sendSpell(user, amount)
    self._claimAndSend(user)
    
    self.total_mSPELL -= amount
    self.mSpellBalanceOf[user] -= amount

@external
def __init__():
    """
    @notice Contract's constructor
    """
    self.total_mSPELL = 0
    self.available_rewards = 0
    self.pending_rewards = 0
    self.contract_mSPELL = MSPELL(mSPELL_ADDRESS)
    self.contract_SPELL = ERC20(SPELL_ADDRESS)
    self.contract_MIM = ERC20(MIM_ADDRESS)

@external
@nonpayable
@nonreentrant("withdraw")
def deposit(amount: uint256):
    """
    @notice Deposits an amount of SPELL into the strategy
    @param amount Amount of SPELL to deposit
    """
    assert amount > 0
    self._deposit(msg.sender, amount)
    
@external
@nonpayable
@nonreentrant("withdraw")
def withdraw(amount: uint256):
    """
    @notice Withdraw an amount of SPELL, this will also claim and transfer the MIMs
    @param amount Amount of SPELL to withdraw
    """
    assert amount > 0
    assert self.mSpellBalanceOf[msg.sender] >= amount
    self._withdraw(msg.sender, amount)
    
@external
@nonpayable
def getUserAmount() -> uint256:
    """
    @notice Claim the MIMs and transfer them to user
    """
    return self.mSpellBalanceOf[msg.sender]

@external
@nonpayable
def claimUserRewards():
    """
    @notice Claim the MIMs and transfer them to user
    """
    self._claimAndSend(msg.sender)

@external
@nonpayable
def getUserShare() -> uint256:
    """
    @notice Get the user's share of the pool
    @return User's share
    """
    return self._getUserShare(msg.sender)

@external
@nonpayable
def strategyHarvest():
    """
    @notice Claim the strategy's pending rewards
    """
    self._strategyHarvest()

@external
@nonpayable
def updateStrategyPendingRewards():
    """
    @notice Update the pending rewards amount if needed
    """
    self._updateStrategyPendingRewards()

@external
@nonpayable
def getUserClaimableAmount(user: address) -> uint256:
    """
    @notice Get the claimable amount of MIM for an user
    @param user User address
    @return User claimable amount of MIMs
    """
    return self._getUserClaimableAmount(user)

# To repay check :
# repay amount <= user_claimable
# debt exists
# repay amount <= debt